FROM coverage-restore AS coverage-build

COPY ./src ./src
COPY ./tests ./tests

RUN dotnet build -c Release --no-restore