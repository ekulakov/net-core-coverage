FROM microsoft/aspnetcore-build:2.0.9-sdk-2.1.202 AS coverage-restore 
WORKDIR /source

COPY ./src/FirstLibrary/FirstLibrary.csproj ./src/FirstLibrary/FirstLibrary.csproj
COPY ./src/SecondLibrary/SecondLibrary.csproj ./src/SecondLibrary/SecondLibrary.csproj

COPY ./tests/FirstLibrary.Tests/FirstLibrary.Tests.csproj ./tests/FirstLibrary.Tests/FirstLibrary.Tests.csproj
COPY ./tests/SecondLibrary.Tests/SecondLibrary.Tests.csproj ./tests/SecondLibrary.Tests/SecondLibrary.Tests.csproj

COPY ./Solution.sln .

RUN dotnet restore Solution.sln