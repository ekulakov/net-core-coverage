#!/bin/bash

set -o pipefail

function dotnet_test {
    dotnet test $1 \
        --no-build \
        -c Release \
        --logger trx \
        -r "../../coverage/tests" \
        /p:CollectCoverage=true \
        /p:CoverletOutputFormat=\"json,opencover\" \
        /p:MergeWith='../../coverage/coverage.json' \
        /p:CoverletOutput='../../coverage/' 2>&1 | tee -a log.txt 
} 

rm -rf coverage/*

echo '{}' > coverage/coverage.json
dotnet_test tests/FirstLibrary.Tests/
dotnet_test tests/SecondLibrary.Tests/

cd tests/FirstLibrary.Tests/

rm -rf coverage/report
dotnet reportgenerator "-reports:../../coverage/coverage.opencover.xml" "-targetdir:../../coverage/report"

cd ../../

perl -0777 -ne 'print ">>>$1<<<\n" if /Total Line: (\d+|\d+.\d+)%(?!.*Total Line:)/s' log.txt

for file in coverage/tests/*.trx
  do xsltproc trx-to-junit.xslt $file > ${file%.*}.xml
done

rm -f coverage/tests/*.trx

if grep "Test Run Failed." log.txt; then
    exit 1
fi