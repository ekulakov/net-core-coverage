using System;
using Xunit;

namespace SecondLibrary.Tests
{
    public class UnitTest1
    {
        [Fact]
        public void ShouldPass()
        {
            Assert.Equal(2, new Class1().Test());
        }
    }
}
