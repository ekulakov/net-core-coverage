FROM coverage-build AS coverage-test

RUN apt-get update && apt-get --assume-yes install xsltproc

COPY ./run_coverage.sh .

COPY ./trx-to-junit.xslt .

CMD ./run_coverage.sh